package org.tds.sgh.system;

import org.tds.sgh.business.CadenaHotelera;
import org.tds.sgh.dtos.ReservaDTO;

public class ModificarReservaController extends ControllerBase implements IModificarReservaController {

	public ModificarReservaController(CadenaHotelera cadenaHotelera) {
		super(cadenaHotelera);
	}

	@Override
	public ReservaDTO seleccionarReserva(long codigoReserva) throws Exception {
		if (codigoReserva < 0)
			throw new Exception("codigo de reserva no valido");
		
		reserva = cadenaHotelera.seleccionarReserva(codigoReserva, cliente);
		return DTO.map(reserva);
	}

}
