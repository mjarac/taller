package org.tds.sgh.system;

import java.util.GregorianCalendar;

import org.tds.sgh.business.CadenaHotelera;
import org.tds.sgh.business.Hotel;

public class TomarReservaController extends ControllerBase implements ITomarReservaController {

	public TomarReservaController(CadenaHotelera cadenaHotelera) {
		super(cadenaHotelera);
	}

	@Override
	public boolean confirmarDisponibilidad(String nombreHotel, String nombreTipoHabitacion,
			GregorianCalendar fechaInicio, GregorianCalendar fechaFin) throws Exception {
		boolean existe = false;
		
		Hotel hotel = null;
		if (this.reserva != null) {
			
			if(!this.reserva.isModificablePorHuesped())
				throw new Exception("La reserva no se puede modificar");
			
			hotel = cadenaHotelera.buscarHotel(this.reserva.getHotel().getNombre());
			hotel.getReservas().remove(this.reserva.getCodigo());
		}
		
		try {
			existe = super.confirmarDisponibilidad(nombreHotel, nombreTipoHabitacion, fechaInicio, fechaFin);
		} finally {
			if (this.reserva != null)
				hotel.getReservas().put(this.reserva.getCodigo(), this.reserva);
		}

		return existe;
	}

}
