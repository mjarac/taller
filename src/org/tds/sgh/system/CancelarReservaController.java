package org.tds.sgh.system;

import java.util.Set;

import org.tds.sgh.business.CadenaHotelera;
import org.tds.sgh.dtos.ReservaDTO;

public class CancelarReservaController extends ControllerBase implements ICancelarReservaController {

	public CancelarReservaController(CadenaHotelera cadenaHotelera) {
		super(cadenaHotelera);
	}

	@Override
	public Set<ReservaDTO> buscarReservasDelCliente() throws Exception {
		return DTO.mapReservas(cliente.getReservasPendientes());
	}

}
