package org.tds.sgh.system;


import java.util.GregorianCalendar;
import java.util.Set;

import org.tds.sgh.business.CadenaHotelera;
import org.tds.sgh.business.Cliente;
import org.tds.sgh.business.Reserva;
import org.tds.sgh.dtos.ClienteDTO;
import org.tds.sgh.dtos.DTO;
import org.tds.sgh.dtos.HotelDTO;
import org.tds.sgh.dtos.ReservaDTO;
import org.tds.sgh.infrastructure.ISistemaFacturacion;
import org.tds.sgh.infrastructure.ISistemaMensajeria;
import org.tds.sgh.infrastructure.Infrastructure;

public class ControllerBase implements IHacerReservaController, IModificarReservaController, ITomarReservaController, ICancelarReservaController, ISistemaFacturacion, ISistemaMensajeria{
	protected final DTO DTO = org.tds.sgh.dtos.DTO.getInstance();
	protected CadenaHotelera cadenaHotelera;
	protected Cliente cliente;
	protected Reserva reserva;
	
	public ControllerBase(CadenaHotelera cadenaHotelera) {
		this.cadenaHotelera= cadenaHotelera;
	}
	
	public ClienteDTO seleccionarCliente(String rut) throws Exception {
		cliente= cadenaHotelera.buscarCliente(rut);
		return DTO.map(cliente);
	}
	public ClienteDTO registrarCliente(String rut, String nombre, String direccion, String telefono, String mail) throws Exception{
		cliente=cadenaHotelera.agregarCliente(rut, nombre, direccion, telefono, mail);
		return DTO.map(cliente);
	}
	
	@Override
	public Set<ClienteDTO> buscarCliente(String patronNombreCliente) {
		return DTO.mapClientes(cadenaHotelera.buscarClientes(patronNombreCliente));
	}
	
	@Override
	public Set<ReservaDTO> buscarReservasDelCliente() throws Exception {
		// TODO Auto-generated method stub
		return DTO.mapReservas(cliente.getReservasPentiendesFuturas());
	}
	
	@Override
	public ReservaDTO seleccionarReserva(long codigoReserva) throws Exception {
		reserva = cadenaHotelera.seleccionarReserva(codigoReserva);
		return DTO.map(reserva);
	}
	@Override
	public Set<ReservaDTO> buscarReservasPendientes(String nombreHotel) throws Exception {
		return DTO.mapReservas(cadenaHotelera.buscarReservasPendientes(nombreHotel));
	}
	@Override
	public ReservaDTO registrarHuesped(String nombre, String documento) throws Exception {
		reserva.registrarHuesped(nombre, documento);
		return DTO.map(reserva);
	}
	@Override
	public ReservaDTO tomarReserva() throws Exception {
		reserva.getHotel().asignarHabitacion(this.reserva);
		reserva.tomarReserva();
		this.enviarMail(reserva.getCliente().getMail(), "", "");
		this.iniciarEstadia(DTO.map(reserva));
		return DTO.map(reserva);
	}
	
	@Override
	public ReservaDTO modificarReserva(String nombreHotel, String nombreTipoHabitacion, GregorianCalendar fechaInicio,
			GregorianCalendar fechaFin, boolean modificablePorHuesped) throws Exception {
		Reserva res = cadenaHotelera.modificarReserva(nombreHotel, nombreTipoHabitacion, fechaInicio, fechaFin, modificablePorHuesped, reserva);
		this.enviarMail(res.getCliente().getMail(), "", "");
		return DTO.map(res);
	}
	@Override
	public boolean confirmarDisponibilidad(String nombreHotel, String nombreTipoHabitacion,
			GregorianCalendar fechaInicio, GregorianCalendar fechaFin) throws Exception {
		return cadenaHotelera.confirmarDisp(nombreHotel, nombreTipoHabitacion, fechaInicio, fechaFin);
	}
	@Override
	public ReservaDTO registrarReserva(String nombreHotel, String nombreTipoHabitacion, GregorianCalendar fechaInicio,
			GregorianCalendar fechaFin, boolean modificablePorHuesped) throws Exception {
		reserva=cadenaHotelera.registrarReserva(nombreHotel, nombreTipoHabitacion, fechaInicio, fechaFin, modificablePorHuesped, cliente);
		this.enviarMail(cliente.getMail(), "", "");
		return DTO.map(reserva);
	}
	@Override
	public Set<HotelDTO> sugerirAlternativas(String pais, String nombreTipoHabitacion, GregorianCalendar fechaInicio,
			GregorianCalendar fechaFin) throws Exception {
		return DTO.mapHoteles(cadenaHotelera.sugerirAlternativas(pais, nombreTipoHabitacion, fechaInicio, fechaFin));
	}
	
	@Override
	public ReservaDTO cancelarReservaDelCliente() throws Exception {
		// TODO Auto-generated method stub
		cliente.getReservasPentiendesFuturas().remove(reserva);
		reserva.cancelarReservaDelCliente();
		this.enviarMail(reserva.getCliente().getMail(), "", "");
		return DTO.map(reserva);
	}
	@Override
	public void enviarMail(String destinatario, String asunto, String mensaje) {
		Infrastructure.getInstance().getSistemaMensajeria().enviarMail(destinatario, asunto, mensaje);
		
	}
	@Override
	public void iniciarEstadia(ReservaDTO reserva) {
		Infrastructure.getInstance().getSistemaFacturacion().iniciarEstadia(reserva);
	}
	

}
