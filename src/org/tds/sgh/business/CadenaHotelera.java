package org.tds.sgh.business;

import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.persistence.*;

@Entity
public class CadenaHotelera
{
	private long id;
	// --------------------------------------------------------------------------------------------
	
	private Map<String, Cliente> clientes;
	
	private Map<String, Hotel> hoteles;
	
	private String nombre;
	
	private Map<String, TipoHabitacion> tiposHabitacion;
	
	// --------------------------------------------------------------------------------------------
	
	public CadenaHotelera(String nombre)
	{
		this.clientes = new HashMap<String, Cliente>();
		
		this.hoteles = new HashMap<String, Hotel>();
		
		this.nombre = nombre;
		
		this.tiposHabitacion = new HashMap<String, TipoHabitacion>();
	}
	
	// --------------------------------------------------------------------------------------------
	
	public Cliente agregarCliente(
		String rut,
		String nombre,
		String direccion,
		String telefono,
		String mail) throws Exception
	{
		if (this.clientes.containsKey(rut))
		{
			throw new Exception("Ya existe un cliente con el RUT indicado.");
		}
		
		Cliente cliente = new Cliente(direccion, mail, nombre, rut, telefono);
		
		this.clientes.put(cliente.getRut(), cliente);
		
		return cliente;
	}
	
	public Hotel agregarHotel(String nombre, String pais) throws Exception
	{
		if (this.hoteles.containsKey(nombre))
		{
			throw new Exception("Ya existe un hotel con el nombre indicado.");
		}
		
		Hotel hotel = new Hotel(nombre, pais);
		
		this.hoteles.put(hotel.getNombre(), hotel);
		
		return hotel;
	}
	
	public TipoHabitacion agregarTipoHabitacion(String nombre) throws Exception
	{
		if (this.tiposHabitacion.containsKey(nombre))
		{
			throw new Exception("Ya existe un tipo de habitación con el nombre indicado.");
		}
		
		TipoHabitacion tipoHabitacion = new TipoHabitacion(nombre);
		
		this.tiposHabitacion.put(tipoHabitacion.getNombre(), tipoHabitacion);
		
		return tipoHabitacion;
	}
	
	public Cliente buscarCliente(String rut) throws Exception
	{
		Cliente cliente = this.clientes.get(rut);
		
		if (cliente == null)
		{
			throw new Exception("No existe un cliente con el nombre indicado.");
		}
		
		return cliente;
	}
	
	public Set<Cliente> buscarClientes(String patronNombreCliente)
	{
		if (patronNombreCliente == null)
			throw new NullPointerException("patronNombreCliente no debe ser null");
		
		Set<Cliente> clientesEncontrados = new HashSet<Cliente>();
		
		for (Cliente cliente : this.clientes.values())
		{
			if (cliente.coincideElNombre(patronNombreCliente))
			{
				clientesEncontrados.add(cliente);
			}
		}
		
		return clientesEncontrados;
	}
	
	public Hotel buscarHotel(String nombre) throws Exception
	{
		Hotel hotel = this.hoteles.get(nombre);
		
		if (hotel == null)
		{
			throw new Exception("No existe un hotel con el nombre indicado.");
		}
		
		return hotel;
	}
	
	public TipoHabitacion buscarTipoHabitacion(String nombre) throws Exception
	{
		TipoHabitacion tipoHabitacion = this.tiposHabitacion.get(nombre);
		
		if (tipoHabitacion == null)
		{
			throw new Exception("No existe un tipo de habitación con el nombre indicado.");
		}
		
		return tipoHabitacion;
	}
	
	public String getNombre()
	{
		return this.nombre;
	}
	
	public Set<Cliente> listarClientes()
	{
		return new HashSet<Cliente>(this.clientes.values());
	}
	
	public Set<Hotel> listarHoteles()
	{
		return new HashSet<Hotel>(this.hoteles.values());
	}
	
	public Set<TipoHabitacion> listarTiposHabitacion()
	{
		return new HashSet<TipoHabitacion>(this.tiposHabitacion.values());
	}
	
	public boolean confirmarDisp(String nombreHotel, String nombreTipoHabitacion, GregorianCalendar fechaInicio, GregorianCalendar fechaFin) throws Exception   
	{
		Hotel hotel= buscarHotel(nombreHotel);
		TipoHabitacion tipoHabitacion= buscarTipoHabitacion(nombreTipoHabitacion);
		return hotel.confirmarDisp(tipoHabitacion, fechaInicio, fechaFin);
		
	}
	
	public Set<Hotel> sugerirAlternativas(String pais, String nombreTipoHabitacion, GregorianCalendar fechaInicio, GregorianCalendar fechaFin) throws Exception{
		Set<Hotel> hotelesAlternativos =new HashSet<Hotel>();
		TipoHabitacion tipoHabitacion=buscarTipoHabitacion(nombreTipoHabitacion);
		
		for (Entry<String, Hotel> entry: hoteles.entrySet()) {
			if (entry.getValue().coincidePais(pais)) {
				if (entry.getValue().confirmarDisp(tipoHabitacion, fechaInicio, fechaFin)) {
					hotelesAlternativos.add(entry.getValue());
				}
			}
		}
		return hotelesAlternativos;
	}
	
	public Reserva registrarReserva(String nombreHotel, String nombreTipoHabitacion, GregorianCalendar fechaInicio, GregorianCalendar fechaFin, boolean modificablePorHuesped, Cliente cliente) throws Exception {
		Hotel hotel = buscarHotel(nombreHotel);
		TipoHabitacion tipoHabitacion= buscarTipoHabitacion(nombreTipoHabitacion);
		Reserva reservaTemp = hotel.crearReserva(tipoHabitacion, fechaInicio, fechaFin,modificablePorHuesped, cliente);
		return reservaTemp; 
	}
	
	public Reserva modificarReserva(String nombreHotel, String nombreTipoHabitacion, GregorianCalendar fechaInicio, GregorianCalendar fechaFin, boolean modificablePorHuesped, Reserva reservaModificar)throws Exception {
		Hotel hotel = buscarHotel(nombreHotel);
		TipoHabitacion tipoHabitacion= buscarTipoHabitacion(nombreTipoHabitacion);
		reservaModificar.modificarReserva(hotel, tipoHabitacion, fechaInicio, fechaFin, modificablePorHuesped);
		return reservaModificar;
	}
	
	public Set<Reserva> buscarReservasPendientes(String nombreHotel) {
		Hotel hotel = hoteles.get(nombreHotel);
		return hotel.buscarReservasPendientes();
	}
	
	public Reserva seleccionarReserva(long codigoReserva) throws Exception {
		Reserva reserva;
			for (Hotel hotel : this.hoteles.values()) {
				reserva = hotel.seleccionarReserva(codigoReserva);
				
				if (reserva != null)
					return reserva;
			}

			throw new Exception ("reserva no existe");
	}
	
	public Reserva seleccionarReserva(long codigoReserva, Cliente cliente) throws Exception {
		if(cliente == null)
			return null;
		
		Reserva reserva = cliente.seleccionarReserva(codigoReserva);
			if (reserva != null)
				return reserva;

			return null;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@OneToMany(cascade=CascadeType.ALL)
	@MapKey(name="rut")
	public Map<String, Cliente> getClientes() {
		return clientes;
	}

	
	public void setClientes(Map<String, Cliente> clientes) {
		this.clientes = clientes;
	}

	@OneToMany(cascade=CascadeType.ALL)
	@MapKey(name="nombre")
	public Map<String, Hotel> getHoteles() {
		return hoteles;
	}

	public void setHoteles(Map<String, Hotel> hoteles) {
		this.hoteles = hoteles;
	}

	@OneToMany(cascade=CascadeType.ALL)
	@MapKey(name="nombre")
	public Map<String, TipoHabitacion> getTiposHabitacion() {
		return tiposHabitacion;
	}

	public void setTiposHabitacion(Map<String, TipoHabitacion> tiposHabitacion) {
		this.tiposHabitacion = tiposHabitacion;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
	
}
