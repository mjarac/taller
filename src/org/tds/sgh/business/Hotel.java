package org.tds.sgh.business;

import java.util.GregorianCalendar;
import javax.persistence.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.tds.sgh.infrastructure.Infrastructure;

@Entity
public class Hotel
{
	private long id;
	// --------------------------------------------------------------------------------------------
	
	private Map<String, Habitacion> habitaciones;
	private Map<Long, Reserva> reservas;
	
	private String nombre;
	
	private String pais;
	
	// --------------------------------------------------------------------------------------------
	
	public Hotel(String nombre, String pais)
	{
		this.habitaciones = new HashMap<String, Habitacion>();
		this.reservas = new HashMap<Long, Reserva>();
		
		this.nombre = nombre;
		
		this.pais = pais;
	}
	
	// --------------------------------------------------------------------------------------------
	
	public Habitacion agregarHabitacion(TipoHabitacion tipoHabitacion, String nombre) throws Exception
	{
		if (this.habitaciones.containsKey(nombre))
		{
			throw new Exception("El hotel ya tiene una habitación con el nombre indicado.");
		}
		
		Habitacion habitacion = new Habitacion(tipoHabitacion, nombre);
		
		this.habitaciones.put(habitacion.getNombre(), habitacion);
		
		return habitacion;
	}
	
	public String getNombre()
	{
		return this.nombre;
	}
	
	public String getPais()
	{
		return this.pais;
	}
	
	public Set<Habitacion> listarHabitaciones()
	{
		return new HashSet<Habitacion>(this.habitaciones.values());
	}
	// --------------------------------------------------------------------------------------------
	
	public boolean confirmarDisp(TipoHabitacion th, GregorianCalendar fi, GregorianCalendar ff) throws Exception {
		
		if (Infrastructure.getInstance().getCalendario().esPasada(fi))
			throw new Exception("fecha pasada");
		
		if (Infrastructure.getInstance().getCalendario().esPasada(ff))
			throw new Exception("fecha pasada");
		
		if(Infrastructure.getInstance().getCalendario().esAnterior(ff, fi))
			throw new Exception("fecha inicio es posterior a fecha de fin");
		
		int cap = contarHabitaciones(th);
		int cr = contarReservas(th, fi, ff);
		return cap > cr;
	}
	
	private int contarHabitaciones(TipoHabitacion th) {
		int contarHabitacion = 0;
		for (Map.Entry<String, Habitacion> entry : this.habitaciones.entrySet()) {
		    if (entry.getValue().coincideTipo(th)) {
		    	contarHabitacion ++;
		    }
		}
		return contarHabitacion;
	}
	
	private int contarReservas(TipoHabitacion th, GregorianCalendar fi, GregorianCalendar ff) {
		int contarReservas = 0;
		for (Map.Entry<Long, Reserva> entry : this.reservas.entrySet()) {
			if (!this.getNombre().equals(entry.getValue().getHotel().getNombre()))
				continue;
			
		    if (entry.getValue().hayConflicto(th, fi, ff)) {
		    	contarReservas ++;
		    }
		}
		return contarReservas;
	}
	
	public boolean coincidePais(String p) {
		return this.pais.equals(p);
	}
	
	public Reserva crearReserva(TipoHabitacion th, GregorianCalendar fi, GregorianCalendar ff, boolean mph, Cliente c) {
		Reserva r = new Reserva(fi,ff,mph, c, th, this);
		this.reservas.put(r.getCodigo(), r);
		c.agregarReserva(r);
		return r;
	}
	
	public Set<Reserva> buscarReservasPendientes() {
		Set<Reserva> rs = new HashSet<Reserva>();
		for (Map.Entry<Long, Reserva> r : this.reservas.entrySet()) {
			if(r.getValue().pendiente()) {
				rs.add(r.getValue());
			}
		}
		return rs;
	}

	public Reserva seleccionarReserva(long codigoReserva) {
		return this.reservas.get(codigoReserva);
	}
	
	public void asignarHabitacion(Reserva reserva) {
		for (Map.Entry<String, Habitacion> eHab : this.habitaciones.entrySet()) {
			if (!reserva.getTipoHabitacion().getNombre().equals(eHab.getValue().getTipoHabitacion().getNombre()))
				continue;
			
			if (!habitacionTomada(eHab.getValue())) {
				reserva.setHabitacion(eHab.getValue());
				break;
			}
		}
	}
	
	private boolean habitacionTomada(Habitacion habitacion) {
		for(Entry<Long, Reserva> eReserva : this.reservas.entrySet())
			if(eReserva.getValue().getEstado() == EstadoReserva.Tomada && eReserva.getValue().getHabitacion().getNombre().equals(habitacion.getNombre()))
				return true;
		
		return false;	
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Hotel [habitaciones=");
		builder.append(habitaciones);
		builder.append(", reservas=");
		builder.append(reservas);
		builder.append(", nombre=");
		builder.append(nombre);
		builder.append(", pais=");
		builder.append(pais);
		builder.append("]");
		return builder.toString();
	}

	@OneToMany(cascade=CascadeType.ALL)
	@MapKey(name="codigo")
	public Map<Long, Reserva> getReservas() {
		return reservas;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@OneToMany(cascade=CascadeType.ALL)
	@MapKey(name="nombre")
	public Map<String, Habitacion> getHabitaciones() {
		return habitaciones;
	}

	public void setHabitaciones(Map<String, Habitacion> habitaciones) {
		this.habitaciones = habitaciones;
	}

	public void setReservas(Map<Long, Reserva> reservas) {
		this.reservas = reservas;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}
	
	
}
