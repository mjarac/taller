package org.tds.sgh.business;


import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.tds.sgh.infrastructure.Infrastructure;

@Entity
public class Cliente {
	private long id;
	
	// --------------------------------------------------------------------------------------------

	private String direccion;

	private String mail;

	private String nombre;

	private String rut;

	private String telefono;

	private Map<Long, Reserva> reservas;
	
	// --------------------------------------------------------------------------------------------
	
	public Cliente(String direccion, String mail, String nombre, String rut, String telefono
			) {
		super();
		this.direccion = direccion;
		this.mail = mail;
		this.nombre = nombre;
		this.rut = rut;
		this.telefono = telefono;
		this.reservas = new HashMap<Long, Reserva>();
	}

	// --------------------------------------------------------------------------------------------

	public boolean coincideElNombre(String patronNombreCliente) {
		return this.nombre.matches(patronNombreCliente);
	}

	public String getDireccion() {
		return this.direccion;
	}

	public String getMail() {
		return this.mail;
	}

	public String getNombre() {
		return this.nombre;
	}

	public String getRut() {
		return this.rut;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void agregarReserva(Reserva reserva) {
		this.reservas.put(reserva.getCodigo(), reserva);
	}

	@Transient
	public Set<Reserva> getReservasPentiendesFuturas() {
		Set<Reserva> reservasEncontrados = new HashSet<Reserva>();
		for (Reserva reserva : this.reservas.values()) {
			if(reserva.pendiente() && (Infrastructure.getInstance().getCalendario().esHoy(reserva.getFechaInicio()) || Infrastructure.getInstance().getCalendario().esFutura(reserva.getFechaInicio())))
				reservasEncontrados.add(reserva);
		}
		return reservasEncontrados;
	}
	
	@Transient
	public Set<Reserva> getReservasPendientes() {
		Set<Reserva> reservasEncontrados = new HashSet<Reserva>();
		for (Reserva reserva : this.reservas.values()) {
			if(reserva.pendiente())
				reservasEncontrados.add(reserva);
		}
		return reservasEncontrados;
	}

	public Reserva seleccionarReserva(Long codReserva) throws Exception {
		Reserva reserva = this.reservas.get(codReserva);

		if (reserva == null) {
			throw new Exception("No existe la reserva indicada.");
		}

		return reserva;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public void setReservas(Map<Long, Reserva> reservas) {
		this.reservas = reservas;
	}
	
	public void setReservasPendientes(Map<Long, Reserva> reservas) {
		this.reservas = reservas;
	}

	@OneToMany(cascade=CascadeType.ALL)
	@MapKey(name="codigo")
	public Map<Long, Reserva> getReservas() {
		return reservas;
	}

	
}
