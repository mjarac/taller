package org.tds.sgh.business;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.tds.sgh.infrastructure.ICalendario;
import org.tds.sgh.infrastructure.Infrastructure;
import javax.persistence.*;

@Entity
public class Reserva {

	private long id;

	public List<Huesped> huespedes;

	private long codigo;
	private GregorianCalendar fechaInicio;
	private GregorianCalendar fechaFin;
	private boolean modificablePorHuesped;
	private EstadoReserva estado;

	private Cliente cliente;
	private TipoHabitacion tipoHabitacion;
	private Habitacion habitacion;

	private Hotel hotel;

	@OneToOne(cascade=CascadeType.ALL)
	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	public Reserva(GregorianCalendar fechaInicio, GregorianCalendar fechaFin, boolean modificablePorHuesped,
			Cliente cliente, TipoHabitacion tipoHabitacion, Hotel hotel) {
		super();
		SecureRandom ran = new SecureRandom();
		this.codigo = Math.abs(ran.nextLong());//System.currentTimeMillis();
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		this.modificablePorHuesped = modificablePorHuesped;
		this.cliente = cliente;
		this.tipoHabitacion = tipoHabitacion;
		this.estado = EstadoReserva.Pendiente;
		this.huespedes = new ArrayList<Huesped>();
		this.hotel = hotel;
	}

	public boolean hayConflicto(TipoHabitacion tipoHabitacion, GregorianCalendar fi, GregorianCalendar ff) {
		
		if (this.getEstado() != EstadoReserva.Pendiente && this.estado != EstadoReserva.Tomada)
			return false;

		if (!this.tipoHabitacion.getNombre().equals(tipoHabitacion.getNombre()))
			return false;

		if (Infrastructure.getInstance().getCalendario().esMismoDia(fi, this.getFechaInicio()))
			return true;

		if (Infrastructure.getInstance().getCalendario().esAnterior(this.getFechaInicio(), fi) && Infrastructure.getInstance().getCalendario().esPosterior(this.getFechaFin(), fi))
			return true;

		if (Infrastructure.getInstance().getCalendario().esMismoDia(ff, this.getFechaFin()))
			return true;

		if (Infrastructure.getInstance().getCalendario().esAnterior(this.getFechaInicio(), ff) && Infrastructure.getInstance().getCalendario().esPosterior(this.getFechaFin(), ff))
			return true;

		if (Infrastructure.getInstance().getCalendario().esAnterior(fi, this.getFechaInicio()) && Infrastructure.getInstance().getCalendario().esPosterior(ff, this.getFechaInicio()))
			return true;

		return false;
	}

	public void modificarReserva(Hotel hotel, TipoHabitacion th, GregorianCalendar fi, GregorianCalendar ff,
			boolean mph) {
		this.getHotel().getReservas().remove(this.getCodigo());
		this.setTipoHabitacion(th);
		this.setFechaInicio(fi);
		this.setFechaFin(ff);
		this.setModificablePorHuesped(mph);
		this.setHotel(hotel);
		hotel.getReservas().put(this.getCodigo(), this);
		
	}

	public boolean pendiente() {
		return this.getEstado() == EstadoReserva.Pendiente;
	}

	public void registrarHuesped(String nombre, String documento) {
		this.getHuespedes().add(new Huesped(nombre, documento));
	}

	public void tomarReserva() {
		this.setEstado(EstadoReserva.Tomada);
	}

	public void cancelarReservaDelCliente() {
		this.setEstado(EstadoReserva.Cancelada);
//		this.hotel.getReservas().get(this.getCodigo()).setEstado(EstadoReserva.Cancelada);
	}

	public long getCodigo() {
		return codigo;
	}

	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}

	public GregorianCalendar getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(GregorianCalendar fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public GregorianCalendar getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(GregorianCalendar fechaFin) {
		this.fechaFin = fechaFin;
	}

	public boolean isModificablePorHuesped() {
		return modificablePorHuesped;
	}

	public void setModificablePorHuesped(boolean modificablePorHuesped) {
		this.modificablePorHuesped = modificablePorHuesped;
	}

	
	public EstadoReserva getEstado() {
		return estado;
	}

	public void setEstado(EstadoReserva estado) {
		this.estado = estado;
	}

	@OneToOne(cascade=CascadeType.ALL)
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	@OneToOne(cascade=CascadeType.ALL)
	public TipoHabitacion getTipoHabitacion() {
		return tipoHabitacion;
	}

	public void setTipoHabitacion(TipoHabitacion tipoHabitacion) {
		this.tipoHabitacion = tipoHabitacion;
	}

	@OneToOne(cascade=CascadeType.ALL)
	public Habitacion getHabitacion() {
		return habitacion;
	}

	public void setHabitacion(Habitacion habitacion) {
		this.habitacion = habitacion;
	}
	
	@OneToMany(cascade=CascadeType.ALL)
	@MapKey(name="nombre")
	public List<Huesped> getHuespedes() {
		return huespedes;
	}

	public void setHuespedes(List<Huesped> huespedes) {
		this.huespedes = huespedes;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Reserva [huespedes=");
		builder.append(huespedes);
		builder.append(", codigo=");
		builder.append(codigo);
		builder.append(", fechaInicio=");
		builder.append(fechaInicio);
		builder.append(", fechaFin=");
		builder.append(fechaFin);
		builder.append(", modificablePorHuesped=");
		builder.append(modificablePorHuesped);
		builder.append(", estado=");
		builder.append(estado);
		builder.append(", cliente=");
		builder.append(cliente);
		builder.append(", tipoHabitacion=");
		builder.append(tipoHabitacion);
		builder.append(", habitacion=");
		builder.append(habitacion);
		builder.append(", hotel=");
		builder.append(hotel);
		builder.append("]");
		return builder.toString();
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
}
